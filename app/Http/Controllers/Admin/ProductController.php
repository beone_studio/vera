<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\ProductsPhoto;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function index(Request $request)
	{
        return Product::orderBy('show_on_main', 'DESC')->orderBy('created_at', 'DESC')->get();
    }
    
    public function getProduct($id)
	{
        return Product::where('id', $id)->with('gallery')->first();
	}

    public function create(Request $request) 
    {
        $product = Product::create([
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'about' => $request->input('about'),
            'size' => $request->input('size'),
            'style' => $request->input('style'),
            'image' => $request->image->store('products', 'public'),
            'order' => $request->input('order'),
            'show_on_main' => $request->input('show_on_main'),
        ]);
        $product->type_id = (int)$request->input('type_id');
        $product->save();

		return ['result' => 'succsess'];
    }
    
    public function addImage($id, Request $request) {

        $product = ProductsPhoto::create([
            'alt' => $request->input('alt'),
            'order' => $request->input('order'), 
            'product_id' => $id,
            'image' => $request->image->store('product' . $id, 'public')
        ]);

		return ['result' => 'succsess'];
	}

	public function update($id, Request $request)
    {   
        $data = [
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'about' => $request->input('about'),
            'size' => $request->input('size'),
            'style' => $request->input('style'),
            'order' => $request->input('order'),
            'show_on_main' => $request->input('show_on_main'),
        ];

        $product = Product::where('id', $id)->first();

        if ($request->hasFile('image')) {
            \Storage::disk('public')->delete($product->image);
            $data['image'] = $request->image->store('products', 'public');
        }

        $product->update($data);
        $product->type_id = (int)$request->input('type_id');
        $product->save();

        return ['result' => 'success'];
    }

    public function delete($id)
    {
        $product=Product::where('id', $id)->first();
        \Storage::disk('public')->delete($product->image);
        \Storage::disk('public')->delete($product->plan_img);
        $product->delete();

        return ['result' => 'success'];
    }

    public function deletePhoto($id)
    {
        $photo=ProductsPhoto::where('id', $id)->first();
        \Storage::disk('public')->delete($photo->image);
        $photo->delete();

        return ['result' => 'success'];
    }
}