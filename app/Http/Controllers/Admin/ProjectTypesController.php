<?php 

namespace App\Http\Controllers\Admin;

use App\ProjectType;

use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class ProjectTypesController extends Controller {

    public function getSubTypes() {

        $types = ProjectType::where('parent_id', null)
                    ->withCount('children')
                    ->get();
        
        return compact('types');
    }

    public function getSubType($id) {

        $type = ProjectType::where('id', $id)->first();

        $parent_id = $type->id;   

        $children = ProjectType::where('parent_id', $parent_id)
                    ->withCount('children')
                    ->get();
        $freeTypes = ProjectType::withCount('children')
                          ->where('id','<>',$parent_id)
                          ->where(function($query) use ($parent_id) {
                                $query->where('parent_id','<>',$parent_id)
                                      ->orWhereNull('parent_id');
                          })
                          ->get();
        
        return compact('type', 'children', 'freeTypes');
    }

    public function getAllTypes() {
        return ProjectType::get();
    }

	public function create(Request $request) {
        $data = [
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'parent_id' => $request->input('parent_id')
        ];

        ProjectType::create($data);

		return ['result' => 'success'];
	}

	public function update(ProjectType $type, Request $request)
    {
        $data = [
            'name' => $request->input('name'),
            'slug' => $request->input('slug'),
            'parent_id' => $request->input('parent_id')
        ];

        $type->update($data);
        $type->parent_id = $request->input('parent_id');
        $type->save();

        return ['result' => 'success'];
    }	

	public function delete($id)
    {
        $type=ProjectType::where('id', $id)->first();
        
        $type->delete();

        return ['result' => 'success'];
    }	
    
}