<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{

    public function send(Request $request) {
        $title = 'Kupriyanova.by | Новая заявка';
        $name = $request->input('firstName', '') . $request->input('secondName', '');
        $email = $request->input('email', '');
        $number = $request->input('number', '');
        $text = $request->input('text', '');

        Mail::send(
            'emails.order',
            [
                'title' => $title,
                'name' => $name,
                'email' => $email,
                'number' => $number,
                'text' => $text
            ],
            function ($message) use ($title)
            {

                $message->from('veradesign@bk.ru', $title);

                $message->to('veradesign@bk.ru')->subject($title);

            }
        );

        return redirect('/')->with('flash_message', 'Ваша заявка принята');

    }
}
