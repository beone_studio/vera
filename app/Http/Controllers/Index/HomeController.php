<?php

namespace App\Http\Controllers\Index;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\Style;
use App\ProjectType;
use App;

class HomeController extends Controller
{
    public function index()
    {
        $styles = Style::orderBy('show_on_main', 'DESC')->orderBy('created_at', 'DESC')->take(6)->get();
        $projects = Product::orderBy('show_on_main', 'DESC')->orderBy('created_at', 'DESC')->take(4)->get();
        $types = ProjectType::where('parent_id', null)
                    ->withCount('children')
                    ->with('children')
                    ->get();

        return view('index.main', compact('styles', 'projects', 'types'));
    }

    public function style($style)
    {
        $style = Style::with('features', 'gallery')->where('slug', $style)->first();
        $types = ProjectType::where('parent_id', null)
                    ->withCount('children')
                    ->with('children')
                    ->get();

        return view('index.style', compact('style', 'types'));
    }

    public function project($project)
    {
        $project = Product::with('gallery')->where('slug', $project)->first();
        $types = ProjectType::where('parent_id', null)
                    ->withCount('children')
                    ->with('children')
                    ->get();

        return view('index.project', compact('project', 'types'));
    }

    public function uslugi()
    {
        $types = ProjectType::where('parent_id', null)
                    ->withCount('children')
                    ->with('children')
                    ->get();

        return view('index.uslugi', compact('types'));
    }

    public function reviews()
    {
        $types = ProjectType::where('parent_id', null)
                    ->withCount('children')
                    ->with('children')
                    ->get();

        return view('index.reviews', compact('types'));
    }
    
    public function portfolio()
    {
        $type = null;
        $projects = Product::with('gallery')
            ->orderBy('order')
            ->get();
        $types = ProjectType::where('parent_id', null)
                    ->withCount('children')
                    ->with('children')
                    ->get();

        return view('index.portfolio', compact('projects', 'type', 'types'));
    }

    public function portfolioSubType($slug)
    {
        $type = ProjectType::where('slug', $slug)
            ->with('children')
            ->withCount('children')
            ->first();
        $types = ProjectType::where('parent_id', null)
                    ->withCount('children')
                    ->with('children')
                    ->get();

        if ($type && $type->children_count > 0) {
            $ids = array();
            foreach ($type->children as $child) {
                array_push($ids, $child->id);
            }

            $projects = Product::whereIn('type_id', $ids)
                ->with('gallery')
                ->orderBy('order')
                ->get();

            return view('index.portfolio', compact('projects', 'type', 'types'));
        } else if ($type) {
            $projects = Product::where('type_id', $type->id)
                ->with('gallery')
                ->orderBy('order')
                ->get();

            return view('index.portfolio', compact('projects', 'type', 'types'));
        } else {

            return redirect('/portfolio');
        }
    }

    public function styles()
    {
        $styles = Style::orderBy('show_on_main', 'DESC')->orderBy('created_at', 'DESC')->get();
        $types = ProjectType::where('parent_id', null)
                    ->withCount('children')
                    ->with('children')
                    ->get();

        return view('index.styles', compact('styles', 'types'));
    }

    public function contact()
    {
        $types = ProjectType::where('parent_id', null)
                    ->withCount('children')
                    ->with('children')
                    ->get();

        return view('index.contact', compact('types'));
    }
}
