<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use URL;
use Illuminate\Cookie\CookieJar;
use Cookie;
use Session;

class LocaleController extends Controller {

    public function setLocale($locale)
    {
        if (! in_array($locale,['en','ru']))
        {
            $locale = 'en';
        }

        return redirect('/')->withCookie(cookie('langName', $locale));
    }
}