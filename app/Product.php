<?php

namespace App;

use App\Style;
use App\ProductsPhoto;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'about',
        'size',
        'style',
        'image',
        'order',
        'show_on_main',
    ];

    protected $appends = ['image_url', 'first_photo'];

    public function gallery()
    {
        return $this->hasMany(ProductsPhoto::class, 'product_id')->orderBy(\DB::raw('LENGTH(`order`), `order`'));
    }

    public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    }

    public function getFirstPhotoAttribute()
    {
        return $this->gallery()->first();
    }

    public function getPlanUrlAttribute()
    {
        return \Storage::disk('public')->url($this->plan_img);
    }

    public function scopeFilter($query, $filter)
    {
         return $query
             ->when($id = array_get($filter, 'category_id'), function ($query) use ($id) {
                 $ids = Category::where('parent_id', $id)->pluck('id')->push($id);
                 return $query->whereIn('category_id', $ids);
             })
             ->when($sort = array_get($filter, 'sort'), function ($query) use ($sort) {
                 switch ($sort) {
                     case 'name':
                       return $query->orderBy('name');
                     default:
                         return $query;
                 }
             })
         ;
    }
}
