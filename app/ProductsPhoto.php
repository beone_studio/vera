<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\Product;

class ProductsPhoto extends Model
{
    protected $fillable = ['product_id', 'alt', 'image', 'order'];
 
    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
