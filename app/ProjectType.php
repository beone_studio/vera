<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectType extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'parent_id'
    ];

    public function children()
    {
        return $this->hasMany(ProjectType::class, 'parent_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'product_type_id');
    }

    public function parent() {
        return $this->belongsTo(ProjectType::class);
    }
}
