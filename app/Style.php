<?php

namespace App;

use App\Product;
use App\StylesImages;
use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'short',
        'about',
        'image',
        'show_on_main',
    ];

    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function features()
    {
        return $this->hasMany(StylesFeatures::class, 'style_id')->orderBy(\DB::raw('LENGTH(`order`), `order`'));
    }

    public function gallery()
    {
        return $this->hasMany(StylesImages::class, 'style_id')->orderBy(\DB::raw('LENGTH(`order`), `order`'));
    }
}
