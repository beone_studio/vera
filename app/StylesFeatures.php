<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StylesFeatures extends Model
{
    protected $fillable = ['text', 'style_id', 'order'];

    public function style()
    {
        return $this->belongsTo(Style::class);
    }
}
