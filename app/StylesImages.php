<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\Style;

class StylesImages extends Model
{
    protected $fillable = ['style_id', 'alt', 'image', 'order'];
 
    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    }
    
    public function style()
    {
        return $this->belongsTo(Style::class);
    }
}
