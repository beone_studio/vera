<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldShowOnMain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('styles', function($table) {
            $table->boolean('show_on_main')->default(false);
        });

        Schema::table('products', function($table) {
            $table->boolean('show_on_main')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function($table) {
            $table->dropColumn('show_on_main');
        });

        Schema::table('styles', function($table) {
            $table->dropColumn('show_on_main');
        });
    }
}
