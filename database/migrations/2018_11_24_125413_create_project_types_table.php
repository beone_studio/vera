<?php

use App\ProjectType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('name');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('project_types', function($table) {
            $table->foreign('parent_id')->references('id')->on('project_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_types', function ($table) {
            $table->dropForeign(['parent_id']);
        });
        Schema::dropIfExists('project_types');
    }
}
