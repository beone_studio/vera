<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPlanAddOrderInProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('order')->unsigned()->nullable();
        });
        Schema::table('products', function ($table) {
            $table->dropColumn('plan_img');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('plan_img')->unsigned()->nullable();
        });
        Schema::table('products', function ($table) {
            $table->dropColumn('type_id');
        });
    }
}
