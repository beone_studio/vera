import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from './layout.vue'

export default [
    {
        path: '/categories',
        component: Layout,
        props: {
            partName: 'Стили',
            icon: 'sort',
        },   
        children: [
            {
                path: '', 
                name: 'Список стилей',
                component: Table, 
                props: {
                    partName: 'Список стилей',
                    icon: 'list',
                    showInMenu: true
                },
            },
            {
                path: 'create', 
                name: 'Создание стиля',
                component: Create,
                props: {
                    partName: 'Создание стиля',
                    icon: 'add',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'Редактирование стиля',
                component: Edit, 
                props: {
                    partName: 'Редактирование стиля',
                    icon: 'edit',
                    showInMenu: false
                }
            }                   
        ] 
    }      
]