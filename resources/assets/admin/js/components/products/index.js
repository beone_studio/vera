import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from './layout.vue'

export default [
    {
        path: '/products',
        component: Layout,
        props: {
            partName: 'Проекты',
            icon: 'library_books',
        },
        children: [ 
            {
                path: '', 
                name: 'list',
                component: Table, 
                props: {
                    partName: 'Список проектов',
                    icon: 'list',
                    showInMenu: true
                }
            },
            {
                path: 'create', 
                name: 'create',
                component: Create,
                props: {
                    partName: 'Создать новый проект',
                    icon: 'add',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'edit',
                component: Edit,
                props: {
                    partName: 'Редактирование проекта',
                    icon: 'edit',
                    showInMenu: false
                }                
            }          
        ]        
    }      
]