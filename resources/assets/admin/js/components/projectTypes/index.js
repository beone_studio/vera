import Create from './create.vue'
import Table from './table.vue'
import Edit from './edit.vue'
import Layout from './layout.vue'

export default [
    {
        path: '/project_types',
        component: Layout,
        props: {
            partName: 'Типы Проектов',
            icon: 'library_books',
        },
        children: [ 
            {
                path: '', 
                name: 'list',
                component: Table, 
                props: {
                    partName: 'Список типов',
                    icon: 'list',
                    showInMenu: true
                }
            },
            {
                path: 'create', 
                name: 'create',
                component: Create,
                props: {
                    partName: 'Добавить тип',
                    icon: 'add',
                    showInMenu: true
                }                
            },
            {
                path: ':id', 
                name: 'edit',
                component: Edit,
                props: {
                    partName: 'Редактирование типа проекта',
                    icon: 'edit',
                    showInMenu: false
                }                
            }          
        ]        
    }      
]