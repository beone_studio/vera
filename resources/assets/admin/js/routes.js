import products from './components/products'
import categories from './components/categories'
import projectTypes from './components/projectTypes'

export default [
	...categories,
    ...products,
    ...projectTypes
]