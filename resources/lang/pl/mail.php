<?php

return [

    'title' => 'Skontaktujemy się z Tobą',
    'name' => 'Imię',
    'phone' => 'Numer telefonu',
    'email' => 'Email',
    'time' => 'O której mam się skontaktować?',
    'message' => 'Twoja wiadomość',
    'send' => 'Wyślij'
];