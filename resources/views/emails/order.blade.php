<html>
<head></head>
<body style="background: rgb(235,235,235); font-size: 16px; color:rgb(30,30,30); border-radius: 8px;">
    <h1 style="background: rgb(120,120,120); text-align: center; color: white; padding: 30px;">{{$title}}</h1>
    <div style="padding: 40px; font-size: 16px;">
        <p><b>Имя:</b> {{$name}}</p>
        <p><b>Email:</b> {{$email}}</p>
        <p><b>Номер телефона:</b> {{$number}}</p>
        <p><b>Сообщение:</b> {{$text}}</p>
    </div>
</body>
</html>