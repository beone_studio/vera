@extends('index.layout')

@section('head')
  <title>Design - Kupriyanova Vera</title>
@endsection

@section('content')

<div class="page-top-image">
    <div class="image-back">
        <img src="/assets/index/img/price.jpg" alt="">
    </div>
    <div class="title page">
      <h1>Контакты</h1>
      <h2>Куприянова Вера, Минск, Беларусь</h2>
    </div>
</div>
<div class="content white link">
  <div class="contacts">
    <form class="contact-form" method="POST" action="/send">
        <input class="short" placeholder="Имя" name="firstName" type="text">
        <input class="short" placeholder="Фамилия" name="secondName" type="text">
        <input placeholder="Email" name="email" type="text">
        <input placeholder="Телефон" name="number" type="text">
        <textarea placeholder="Ваше сообщение" name="text" id="" cols="30" rows="4"></textarea>
        <button type="submit">отправить</button>
    </form>
    <div class="info">
      <h3>Жду вашего звонка</h3>
      <p>
        Необходима консультация? Желаете заказать дизайн-проект? Звоните в любой день до 20:00. 
        Буду рада ответить на все Ваши вопросы.
      </p> 
      <p>
        c 10:00 до 20:00
        <br>  
        Каждый день. Без выходных.
      </p>
      <a href="">+375 29 159-45-81</a>
    </div>
  </div>
</div>
<div class="content">
  <div class="footer">
    <div class="text">© 2018 Вера Куприянова. Все права защищены.</div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
@endsection