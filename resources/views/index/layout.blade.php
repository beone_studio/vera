<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru-RU" lang="ru-RU">
	<head>
		@yield('head')
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="description" content="Услуги по разработке дизайна интерьера и 3-д визуализации;">
		<meta name="keywords" content="разработка,создание,проект,планировочное решение,дизайн интерьера,дизайн-проект,рабочие чертежи,3d визуализация,мебель,авторский надзор,строительная компания,отделочные материалы,гостиная,спальня,кухня,детская,ванная">
		<meta name="generator" content="Parallels Presence Builder 12.0.7">
		<link rel="stylesheet" href="/assets/index/css/font-awesome.min.css">
		<link rel="stylesheet" href="/assets/index/css/animate.css">
		<link rel="stylesheet" href="{{ mix('/assets/index/css/app.css') }}">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
		<script src="/assets/index/js/wow.min.js"></script>
		<script src="/assets/index/js/siema.min.js"></script>
	</head>
	<body>
		<div class="burger-menu" id="menu-open">
			<span class="fa fa-bars"></span>
		</div>
		<header>
			<div class="wrapper">
				<div class="inner">
					<a href="/">
						<img width="180" src="/assets/index/img/logo.png" alt="">
						<img width="180" src="/assets/index/img/logo_name.png" alt="">
					</a>
					<nav>
						<li><a href="/">Главная</a></li>
						<li>
							<a href="/portfolio">Портфолио</a>
							<div class="hidden-menu">
								<ul>
									@foreach($types as $link)
										<li>
											<a href="/portfolio/{{$link->slug}}">
												<span>
													{{ $link->name }}
													<div class="line"></div>
												</span>
												@if($link->children_count) 
													<i class="fa fa-angle-right"></i>
												@endif
											</a>
											@if ($link->children_count)
												<div class="hidden-menu">
													<ul>
														@foreach($link->children as $childlink)
															<li>
																<a href="/portfolio/{{$childlink->slug}}">
																	<span>
																		{{ $childlink->name }}
																		<div class="line"></div>
																	</span>
																</a>
															</li>
														@endforeach
													</ul>
												</div>
											@endif
										</li>
									@endforeach
								</ul>
							</div>
						</li>
						<li><a href="/uslugi-i-ceny">Услуги и цены</a></li>
						<li><a href="/styles">Стили</a></li>
						<li><a href="/reviews">Отзывы</a></li>
						<li><a href="/contacts">Контакты</a></li>
					</nav>
					<div class="tel">
						<a href="#">+375 29 159 45 81</a>
					</div>
					<div class="soc-networks">
						<a href="mailto: veradesign@bk.ru"><span class="fa fa-envelope"></span></a>
						<a target="_blank" href="https://www.facebook.com/kupriyanova.by/"><span class="fa fa-facebook"></span></a>
						<a target="_blank" href="https://www.instagram.com/veradesignminsk/?hl=ru"><span class="fa fa-instagram"></span></a>
					</div>
				</div>
			</div>
		</header>
		<div class="main-content">
			<div class="container">
				@yield('content')
			</div>
		</div>
	</body>
	<script>
		new WOW().init();
		window.onload = function() {
			$("#menu-open").click(function() {
				$("header").toggleClass('open');
				$("#menu-open > span").toggleClass('light');
			});
		};
	</script>
</body>
</html>