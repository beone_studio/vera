@extends('index.layout')

@section('head')
  <title>Design - Kupriyanova Vera</title>
	<link rel="stylesheet" href="/assets/index/css/easyzoom.css" />
@endsection

@section('content')
<div class="page-top-image">
    <div class="image-back">
        <img src="/assets/index/img/price.jpg" alt="">
    </div>
    <div class="title page">
        <h1>Портфолио</h1>
        @if ($type)
            <h2>{{ $type->name }}</h2>
        @else
            <h2>Куприянова Вера, Минск, Беларусь</h2>
        @endif
    </div>
</div>
<div class="content white link">
    <div class="block">
        <h3 class="center-title">мои работы</h3>
        <h5 class="center-subtitle">
            На данной странице представлена часть разработанных мною проектов.
            Каждый дизайн-проект создан исходя из пожеланий и вкусовых предпочтений клиента. И
            помните, что безграничную фантазию может ограничить лишь бюджет на ремонт, а безграничное
            число пожеланий – размер помещения.
        </h5>
        <div class="line"></div>
    </div>
    <div class="project-types-menu">
        <ul>
            @foreach($types as $link)
                <li>
                    <a href="/portfolio/{{$link->slug}}">
                        <span>
                            {{ $link->name }}
                        </span>
                    </a>
                    @if ($link->children_count)
                        <ul>
                            @foreach($link->children as $childlink)
                                <li>
                                    <a href="/portfolio/{{$childlink->slug}}">
                                        <span>
                                            {{ $childlink->name }}
                                        </span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
    @foreach($projects as $key=>$project)
        <div class="project-item">
            <h2>{{ $project->name }}</h2>
            <h6>{{ $project->style }} &ndash; {{ $project->size }} м<sup>2</sup></h6>
            <div class="project-preview mt">
                <div class="easyzoom {{ $key }} easyzoom--overlay easyzoom--with-thumbnails">
                    @if(!empty($project->first_photo))
                    <a href="{{$project->first_photo->image_url}}">
                        <img src="{{$project->first_photo->image_url}}" alt=""/>
                    </a>
                    @endif
                </div>

                <ul class="thumbnails {{ $key }}">
                    @foreach($project->gallery as $key=>$photo)
                        @if($key < 9)
                            <li>
                                <a href="{{ $photo->image_url }}" data-standard="{{ $photo->image_url }}">
                                    <img src="{{ $photo->image_url }}" alt="{{ $project->name }}. {{ $photo->alt }}" />
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
                <a target="_blank" href="/project/{{$project->slug}}">подробнее</a>
            </div>
        </div>
    @endforeach
</div>
<div class="content">
    <div class="footer">
      <div class="text">© 2018 Вера Куприянова. Все права защищены.</div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="/assets/index/js/easyzoom.js"></script>
<script>
    // Instantiate EasyZoom instances
    var $easyzoom = $('.easyzoom').easyZoom();

    for (var i = 0; i < Object.keys($easyzoom).length; i++) {
        let api = $easyzoom.filter('.easyzoom--with-thumbnails.' + i).data('easyZoom');  
        $('.thumbnails.' + i).on('click', 'a', function(e) {
            var $this = $(this);

            e.preventDefault();

            // Use EasyZoom's `swap` method
            api.swap($this.data('standard'), $this.attr('href'));
        });
    }
</script>
@endsection