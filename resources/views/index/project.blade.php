@extends('index.layout')

@section('head')
  <title>Design - Kupriyanova Vera</title>
  <link href="/assets/index/css/simplelightbox.css" rel="stylesheet" type="text/css">
@endsection

@section('content')

<div class="page-top-image">
    <div class="image-back">
        <img src="{{$project->first_photo->image_url}}" alt="Дизайн. Минск. Вера Куприянова. {{ $project->name }}">
    </div>
    <div class="title page">
        <h1>{{ $project->name }}</h1>
        <h2>{{ $project->style }} &ndash; {{ $project->size }} м<sup>2</sup></h2>
    </div>
</div>
<div class="content white">
  <div class="block">
    <div class="styles white">
      <div class="info-block wide">
        <h2>ВИЗУАЛИЗАЦИИ ПРОЕКТА</h2>
        <div class="line left"></div>
        <div class="info-block white project">
          <div>
                  {!! $project->about !!}
          </div>
          <a class="review-btn" href="/reviews">оставить отзыв</a>
        </div>
      </div>
    </div>
    <div class="content link">
      <div class="gallery-block">
        <div class="gallery">
          @foreach($project->gallery as $photo)
            <a href="{{ $photo->image_url }}"><img src="{{ $photo->image_url }}" alt="" title="{{ $project->name }}. {{ $photo->alt }}" /></a>
          @endforeach
        </div>
      </div>
    </div>
    <div class="footer" style="margin-top: 20px">
      <div class="text">© 2018 Вера Куприянова. Все права защищены.</div>
    </div>
  </div>
  
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="/assets/index/js/simple-lightbox.min.js"></script>
<script>
  $(function(){
		$('.gallery a').simpleLightbox();
  });
</script>
@endsection