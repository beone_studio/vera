@extends('index.layout')

@section('head')
  <title>Design - Kupriyanova Vera</title>
  <link href="/assets/index/css/simplelightbox.css" rel="stylesheet" type="text/css">
@endsection

@section('content')

<div class="page-top-image">
    <div class="image-back">
        <img src="/assets/index/img/price.jpg" alt="">
    </div>
    <div class="title page">
      <h1>{{ $style->name }}</h1>
    </div>
</div>
<div class="content white link">
  <div class="text-block">
    <h5>Описание</h5>  
    <p>
      {{ $style->about }}
    </p>
    <h5>Концептуальное решение</h5>
    <ul>
      @foreach($style->features as $feature) 
        <li>{{ $feature->text }}</li>
      @endforeach
    </ul>
    <h5>Примеры</h5>
  </div>
  <div class="gallery">
      @foreach($style->gallery as $photo) 
			  <a href="{{ $photo->image_url }}"><img src="{{ $photo->image_url }}" alt="{{ $photo->alt }}"/></a>
      @endforeach
      <div class="clear"></div>
  </div>
</div>
<div class="content">
  <div class="footer">
    <div class="text">© 2018 Вера Куприянова. Все права защищены.</div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="/assets/index/js/simple-lightbox.min.js"></script>
<script>
  $(function(){
		var $gallery = $('.gallery a').simpleLightbox();
	});
</script>
@endsection