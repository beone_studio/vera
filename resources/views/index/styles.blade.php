@extends('index.layout')

@section('head')
  <title>Design - Kupriyanova Vera</title>
	<link rel="stylesheet" href="/assets/index/css/easyzoom.css" />
@endsection

@section('content')
<div class="page-top-image">
    <div class="image-back">
        <img src="/assets/index/img/price.jpg" alt="">
    </div>
    <div class="title page">
        <h1>Cтили</h1>
        <h2>Куприянова Вера, Минск, Беларусь</h2>
    </div>
</div>
<div class="content white link">
    <div class="block">
        <h3 class="center-title">Популярные стили</h3>
        <h5 class="center-subtitle">
            Стиль оказывает влияние на планировку помещений, 
            цветовую гамму, материал исполнения предметов интерьера, 
            их расположение, форму, стоимость самого ремонта.
        </h5>
        <div class="line"></div>
    </div>
    <div class="styles-container">
        @foreach ($styles as $style)
            <div class="style">
                <a target="_blank" href="/styles/{{ $style->slug }}">
                <div class="img-block">
                    <img src="{{ $style->image_url }}" alt="{{ $style->name }}. Популярные стили.">
                    <div class="backdrop">
                        <div class="border">
                        <h5>подробнее</h5>
                    </div>
                    </div>
                </div> 
                </a>
                <h4><a target="_blank" href="/styles/{{ $style->slug }}">{{ $style->name }}</a></h4>
            </div>
        @endforeach
    </div>
</div>
<div class="content">
    <div class="footer">
      <div class="text">© 2018 Вера Куприянова. Все права защищены.</div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
@endsection