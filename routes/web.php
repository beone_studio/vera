<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/send', 'EmailController@send');


Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'AuthController@login');
    Route::post('/', 'AuthController@doLogin');

    Route::group(['middleware' => 'auth'], function () {
        Route::group(['prefix' => 'api'], function () {
            Route::get('/logout', 'AuthController@logout');
            Route::post('/features', 'CategoriesController@createFeature');
            Route::get('/allCategories', 'CategoriesController@getAllCategories');
            Route::delete('/features/{id}', 'CategoriesController@deleteFeature');
            Route::get('/allProjectTypes', 'ProjectTypesController@getAllTypes');

            Route::group(['prefix' => 'categories'], function () {
                Route::get('/', 'CategoriesController@getAllCategories');
                Route::get('/{category}', 'CategoriesController@getCategory');
                Route::delete('/{id}', 'CategoriesController@delete');
                Route::post('/', 'CategoriesController@create');
                Route::post('/{category}', 'CategoriesController@update');
                Route::post('/{category}/image', 'CategoriesController@addImage');
                Route::delete('/image/{id}', 'CategoriesController@deletePhoto');
            });

            Route::group(['prefix' => 'project_types'], function () {
                Route::get('/', 'ProjectTypesController@getSubTypes');
                Route::get('/{project_type}', 'ProjectTypesController@getSubType');
                Route::delete('/{id}', 'ProjectTypesController@delete');
                Route::post('/', 'ProjectTypesController@create');
                Route::post('/{project_type}', 'ProjectTypesController@update');
            });

            Route::group(['prefix' => 'products'], function () {
                Route::get('/', 'ProductController@index');
                Route::get('/{product}', 'ProductController@getProduct');
                Route::post('/', 'ProductController@create');
                Route::post('/{product}/image', 'ProductController@addImage');
                Route::post('/{product}', 'ProductController@update');
                Route::delete('/{product}', 'ProductController@delete');
                Route::delete('/image/{id}', 'ProductController@deletePhoto');
            });
        });

        Route::any('{all?}', function () {
            return view('admin.main');
        })->where(['all' => '.*']);
    });
});

Route::group(['namespace' => 'Index'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/styles/{style}', 'HomeController@style');
    Route::get('/project/{project}', 'HomeController@project');
    Route::get('/uslugi-i-ceny', 'HomeController@uslugi');
    Route::get('/portfolio', 'HomeController@portfolio');
    Route::get('/portfolio/{slug}', 'HomeController@portfolioSubType');
    Route::get('/styles', 'HomeController@styles');
    Route::get('/contacts', 'HomeController@contact');
    Route::get('/reviews', 'HomeController@reviews');
});

Route::get('locale/{locale?}',
    [
        'as' => 'locale.setlocale',
        'uses' => 'LocaleController@setLocale'
    ]);

